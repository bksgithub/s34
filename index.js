const express = require("express")

const app = express()

const port = 4000

app.use(express.json())

app.use(express.urlencoded({exnteded: true}))

app.listen(port, () => {
	console.log(`Server is running at localhost:${port}`)
})

/*
1. Create a GET route that will access the /home route that will print out
a simple message.
*/

app.get("/home", (request, response) => {
	response.send(`This is the homepage.`)
})

/*
2. Process a GET request at the /home route using postman - uploaded screenshot on git repository*/

/*
3. Create a GET route that will access the /users route that will retrieve
all the users in the mock database.
*/ 

let mockDatabase = [
	{
		"user1": "Benneth"
	},
	{
		"user2": "Kyle"
	},
	{
		"user3": "Serrato"
	}
]

app.get("/users", (request, response) => {
	response.json(mockDatabase)
})

/*
5. Create a DELETE route that will access the /delete-user route to
remove a user from the mock database.*/

app.delete("/delete-user", (request, response) => {
	mockDatabase.pop()
	response.end(`A user has been deleted from the mock database.`)
})